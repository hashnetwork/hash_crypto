# 关于加密相关的各种小工具

## RSA相关算法

1、 生成rsa公秘钥对：

```生成rsa公秘钥对 go
// 秘钥的序列化标准：x509 PKCS8
pri, pub, e := GenWithPKCS8(1024)
	if e != nil {
		fmt.Println(e.Error())
		return
	}
_ = os.WriteFile("./private.pem", pri, 0666)
_ = os.WriteFile("./pub.pem", pub, 0666)
```

2、签名和验证 

```用秘钥签名，公钥验签 go
data := "haha"
sign, e1 := RsaPKCS8Sign([]byte(data), pri)
if e1 != nil {
    fmt.Println(e1.Error())
    return
}
signBase64 := base64.StdEncoding.EncodeToString(sign)
fmt.Println("sign", signBase64)

e2 := RSARsa256Verify([]byte(data), signBase64, pub)
if e2 != nil {
    fmt.Println("error:", e2.Error())
    return
}
```

3、加密解密

```rsa加解密 go
data := "haha"
// 加密
encryptData := RSAEncrypt([]byte(data), []byte(pubPem))
//解密
srcData, e := RsaPKCS8Decrypt(encryptData, []byte(priPem))
if e != nil {
    fmt.Println(e.Error())
    return
}
```

## hash算法

1、 sha1算法

```sha1算法 go
data := "haha"
r := Sha1([]byte(data))
fmt.Println(r)
```

2、sha256算法

```sha256算法 go
data := "haha"
r := Sha256([]byte(data))
fmt.Println(r)
```

3、md5算法

``` md5算法 go
data := "haha"
r := Md5([]byte(data))
fmt.Println(r)
```
