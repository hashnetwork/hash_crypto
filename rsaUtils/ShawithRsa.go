// rsa算法，属于非对称加密算法，公钥pk是公开的，秘钥sk是需要保密的
// 公秘钥对生成：是一个标准的生成算法，只需要输入一个秘钥位数，就能生成一个标准的rsa公秘钥对。
// golang生成方法：
//     privateKey, err := rsa.GenerateKey(rand.Reader, bits)；
//     publicKey := privateKey.PublicKey
// 公秘钥对序列化：就是把公秘钥对在程序中的对象，序列化成标准的格式，可供任意的应用程序反序列化。
// 常见的序列化方法是x509的pem格式，格式类似为：
//-----BEGIN CERTIFICATE-----
//<此处为 base64 内容>
//-----END CERTIFICATE-----
//-----BEGIN CERTIFICATE-----
//<此处为 base64 内容>
//-----END CERTIFICATE-----
// 注意：RFC2045中规定：Base64一行不能超过76字符，超过则添加回车换行符，因此证书结果应该是这样的：
//-----BEGIN RSA Public Key-----
//MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC8VQExERfjO7XeLBCE/TpfXFLm
//kFIPTAbFkAcp5Ac8khhMvl3wsJkKxOOJrWNQWDtAZZR8NrJSouI5uUP5xu6E+KUa
//8tjGMvOhrFjYhtD9j9+eVua+nSKZ+xRURkSUR4ZwcHwGg4ycknfV27zuGTCbTLn1
//zm2ko/Ne5VV3OlDC8wIDAQAB
//-----END RSA Public Key-----
//
// 中间的base64格式的ascii码字符串的原始值是序列化后的二进制，生成这个二进制序列的方法有几种标准，
// 常用的有sha1PKCS1，sha1PKCS8。
//
// golang中序列化方式的实现：
//
//  // X509PrivateKey := x509.MarshalPKCS1PrivateKey(privateKey) //或者
//	X509PrivateKey, _ := x509.MarshalPKCS8PrivateKey(privateKey)
//	//使用pem格式对x509输出的内容进行编码
//	//创建文件保存私钥
//	var privateBuff bytes.Buffer
//	var publicBuff bytes.Buffer
//
//	//构建一个pem.Block结构体对象
//	privateBlock := pem.Block{Type: "RSA Private Key", Bytes: X509PrivateKey}
//	//将数据保存到文件
//	_ = pem.Encode(&privateBuff, &privateBlock)
//	publicKey := privateKey.PublicKey
//	//X509对公钥编码
//	X509PublicKey, err := x509.MarshalPKIXPublicKey(&publicKey)
//	if err != nil {
//		return nil, nil, errors.New("生成rsa失败")
//	}
//	//创建一个pem.Block结构体对象
//	publicBlock := pem.Block{Type: "RSA Public Key", Bytes: X509PublicKey}
//	//保存到文件
//	_ = pem.Encode(&publicBuff, &publicBlock)

package rsaUtils

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
)

import (
	"bytes"
	"crypto"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"os"
)

// GenWithPKCS1 生成RSA私钥和公钥，秘钥的x509序列化格式：PKCS1
func GenWithPKCS1(bits int) (priKey []byte, pubKey []byte, e error) {
	//GenerateKey函数使用随机数据生成器random生成一对具有指定字位数的RSA密钥
	//Reader是一个全局、共享的密码用强随机数生成器
	privateKey, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		return nil, nil, errors.New("生成rsa失败")
	}
	//保存私钥
	//通过x509标准将得到的ras私钥序列化为ASN.1 的 DER编码字符串
	X509PrivateKey := x509.MarshalPKCS1PrivateKey(privateKey)
	//使用pem格式对x509输出的内容进行编码
	//创建文件保存私钥
	//privateFile, err := os.Create("private.pem")
	//if err != nil {
	//	panic(err)
	//}
	//defer privateFile.Close()

	var privateBuff bytes.Buffer
	var publicBuff bytes.Buffer

	//构建一个pem.Block结构体对象
	privateBlock := pem.Block{Type: "RSA Private Key", Bytes: X509PrivateKey}
	//将数据保存到文件
	_ = pem.Encode(&privateBuff, &privateBlock)
	//保存公钥
	//获取公钥的数据
	publicKey := privateKey.PublicKey
	//X509对公钥编码
	X509PublicKey, err := x509.MarshalPKIXPublicKey(&publicKey)
	if err != nil {
		return nil, nil, errors.New("生成rsa失败")
	}
	//pem格式编码
	//创建用于保存公钥的文件
	//publicFile, err := os.Create("public.pem")
	//if err != nil {
	//	panic(err)
	//}
	//defer publicFile.Close()
	//创建一个pem.Block结构体对象
	publicBlock := pem.Block{Type: "RSA Public Key", Bytes: X509PublicKey}
	//保存到文件
	_ = pem.Encode(&publicBuff, &publicBlock)

	return privateBuff.Bytes(), publicBuff.Bytes(), nil
}

// GenWithPKCS8 生成RSA私钥和公钥，秘钥的x509序列化标准：PKCS8
func GenWithPKCS8(bits int) (priKey []byte, pubKey []byte, e error) {
	//GenerateKey函数使用随机数据生成器random生成一对具有指定字位数的RSA密钥
	//Reader是一个全局、共享的密码用强随机数生成器
	privateKey, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		return nil, nil, errors.New("生成rsa失败")
	}
	//保存私钥
	//通过x509标准将得到的ras私钥序列化为ASN.1 的 DER编码字符串
	//X509PrivateKey := x509.MarshalPKCS1PrivateKey(privateKey)
	X509PrivateKey, _ := x509.MarshalPKCS8PrivateKey(privateKey)
	//使用pem格式对x509输出的内容进行编码
	//创建文件保存私钥
	//privateFile, err := os.Create("private.pem")
	//if err != nil {
	//	panic(err)
	//}
	//defer privateFile.Close()

	var privateBuff bytes.Buffer
	var publicBuff bytes.Buffer

	//构建一个pem.Block结构体对象
	privateBlock := pem.Block{Type: "RSA Private Key", Bytes: X509PrivateKey}
	//将数据保存到文件
	_ = pem.Encode(&privateBuff, &privateBlock)
	//保存公钥
	//获取公钥的数据
	publicKey := privateKey.PublicKey
	//X509对公钥编码
	X509PublicKey, err := x509.MarshalPKIXPublicKey(&publicKey)
	if err != nil {
		return nil, nil, errors.New("生成rsa失败")
	}
	//pem格式编码
	//创建用于保存公钥的文件
	//publicFile, err := os.Create("public.pem")
	//if err != nil {
	//	panic(err)
	//}
	//defer publicFile.Close()
	//创建一个pem.Block结构体对象
	publicBlock := pem.Block{Type: "RSA Public Key", Bytes: X509PublicKey}
	//保存到文件
	_ = pem.Encode(&publicBuff, &publicBlock)

	return privateBuff.Bytes(), publicBuff.Bytes(), nil
}

// RSAEncrypt RSA公钥加密
func RSAEncrypt(plainText []byte, pubPem []byte) []byte {
	//打开文件

	//pem解码
	block, _ := pem.Decode(pubPem)
	//x509解码
	publicKeyInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		panic(err)
	}
	//类型断言
	publicKey := publicKeyInterface.(*rsa.PublicKey)
	//对明文进行加密
	cipherText, err := rsa.EncryptPKCS1v15(rand.Reader, publicKey, plainText)
	if err != nil {
		panic(err)
	}
	//返回密文
	return cipherText
}

// RsaPKCS8Decrypt RSA解密,秘钥标准：PKCS8
func RsaPKCS8Decrypt(cipherText []byte, priPem []byte) ([]byte, error) {

	//X509解码
	privateKey, err := ParsePKCS8PrivateKey(priPem)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	//对密文进行解密
	plainText, e := rsa.DecryptPKCS1v15(rand.Reader, privateKey, cipherText)
	if e != nil {
		fmt.Println(e.Error())
		return nil, e
	}
	//返回明文
	return plainText, nil
}

// ReadParsePublicKey 读取公钥文件，解析出公钥对象
func ReadParsePublicKey(filename string) (*rsa.PublicKey, error) {
	//--1.读取公钥文件，获取公钥字节
	publicKeyBytes, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	//--2.解码公钥字节，生成加密对象
	block, _ := pem.Decode(publicKeyBytes)
	if block == nil {
		return nil, errors.New("公钥信息错误")
	}
	//--3.解析DER编码的公钥,生成公钥接口
	publicKeyInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	//--4.公钥接口转型成公钥对象
	publicKey := publicKeyInterface.(*rsa.PublicKey)
	return publicKey, nil
}

// ParsePublicKey 解析出公钥对象
func ParsePublicKey(pubCert []byte) (*rsa.PublicKey, error) {
	//--1.读取公钥文件，获取公钥字节
	publicKeyBytes := pubCert
	//--2.解码公钥字节，生成加密对象
	block, _ := pem.Decode(publicKeyBytes)
	if block == nil {
		return nil, errors.New("公钥信息错误")
	}
	//--3.解析DER编码的公钥,生成公钥接口
	publicKeyInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	//--4.公钥接口转型成公钥对象
	publicKey := publicKeyInterface.(*rsa.PublicKey)
	return publicKey, nil
}

// ParsePKCS8PrivateKey 解析出私钥对象
func ParsePKCS8PrivateKey(priPem []byte) (*rsa.PrivateKey, error) {
	//--2.对私钥文件进行编码,生成加密对象
	block, _ := pem.Decode(priPem)
	if block == nil {
		return nil, errors.New("私钥信息错误")
	}
	//3.解析DER编码的私钥,生成私钥对象
	//x509.ParsePKCS8PrivateKey()
	privateKey, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	return privateKey.(*rsa.PrivateKey), err
}

// ReadParsePKCS8PrivateKey 读取私钥文件，解析出私钥对象
func ReadParsePKCS8PrivateKey(filename string) (*rsa.PrivateKey, error) {
	//--1.读取私钥文件，获取私钥字节
	privateKeyBytes, _ := os.ReadFile(filename)
	//--2.对私钥文件进行编码,生成加密对象
	block, _ := pem.Decode(privateKeyBytes)
	if block == nil {
		return nil, errors.New("私钥信息错误")
	}
	//3.解析DER编码的私钥,生成私钥对象
	//x509.ParsePKCS8PrivateKey()
	privateKey, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}
	return privateKey.(*rsa.PrivateKey), err
}

func RSASignWithPriKey(data []byte, privateKey *rsa.PrivateKey) ([]byte, error) {
	//1.选择hash算法，对需要签名的数据进行hash运算
	myHash := crypto.SHA256
	hashInstance := myHash.New()
	hashInstance.Write(data)
	hashed := hashInstance.Sum(nil)

	//hashed := sha256.Sum256(data)
	//--2.对私钥文件进行编码,生成加密对象

	//3.RSA数字签名
	dataBuff, err := rsa.SignPKCS1v15(rand.Reader, privateKey, myHash, hashed)
	if err != nil {
		return nil, err
	}
	return dataBuff, nil
}

// RsaPKCS8Sign2Str rsa签名，私钥标准pkc8,返回标准的base64格式字符串
func RsaPKCS8Sign2Str(data []byte, privatePemKey []byte) (string, error) {
	sign, e := RsaPKCS8Sign(data, privatePemKey)
	if e != nil {
		return "", e
	}
	signBase64 := base64.StdEncoding.EncodeToString(sign)
	return signBase64, nil
}

func RsaPKCS8Sign(data []byte, privatePemKey []byte) ([]byte, error) {
	//1.选择hash算法，对需要签名的数据进行hash运算
	//myhash := crypto.SHA256
	//hashInstance := myhash.New()
	//hashInstance.Write(data)
	//hashed := hashInstance.Sum(nil)
	//--2.对私钥文件进行编码,生成加密对象
	block, _ := pem.Decode(privatePemKey)
	if block == nil {
		return nil, errors.New("私钥信息错误")
	}
	//3.解析DER编码的私钥,生成私钥对象
	privateKey, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	//3.RSA数字签名
	return RSASignWithPriKey(data, privateKey.(*rsa.PrivateKey))
}

// RSASha256Verify 公钥验证数据签名是否正确
func RSASha256Verify(data []byte, base64Sig string, publicPemKey []byte) error {

	//--2.解码公钥字节，生成加密对象
	block, _ := pem.Decode(publicPemKey)
	if block == nil {
		return errors.New("公钥信息错误")
	}
	//--3.解析DER编码的公钥,生成公钥接口
	publicKeyInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return err
	}
	//--4.公钥接口转型成公钥对象
	publicKey := publicKeyInterface.(*rsa.PublicKey)

	//- RSA验证数字签名
	return RSASha256VerifyCore(data, base64Sig, publicKey)
	//return rsa.VerifyPKCS1v15(publicKey, myhash, hashed, buffData)
}

// RSASha256VerifyCore 公钥验证数据签名是否正确
func RSASha256VerifyCore(data []byte, base64Sig string, publicKey *rsa.PublicKey) error {
	//- 对base64编码的签名内容进行解码，返回签名字节
	buffData, err := base64.StdEncoding.DecodeString(base64Sig)
	if err != nil {
		return err
	}
	//- 选择hash算法，对需要签名的数据进行hash运算
	myhash := crypto.SHA256
	hashInstance := myhash.New()
	hashInstance.Write(data)
	hashed := hashInstance.Sum(nil)
	//- RSA验证数字签名
	return rsa.VerifyPKCS1v15(publicKey, myhash, hashed, buffData)
}
