package rsaUtils

import (
	"fmt"
	"os"
	"testing"
)

func TestSlice(t *testing.T) {
	pri, pub, e := GenWithPKCS8(1024)
	if e != nil {
		fmt.Println(e.Error())
		return
	}
	_ = os.WriteFile("./private.pem", pri, 0666)
	_ = os.WriteFile("./pub.pem", pub, 0666)

}
