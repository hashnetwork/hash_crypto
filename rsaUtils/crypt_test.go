package rsaUtils

import (
	"fmt"
	"testing"
)

//加解密测试

func TestCrypt(t *testing.T) {
	priPem := `-----BEGIN RSA Private Key-----
MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBALxVATERF+M7td4s
EIT9Ol9cUuaQUg9MBsWQBynkBzySGEy+XfCwmQrE44mtY1BYO0BllHw2slKi4jm5
Q/nG7oT4pRry2MYy86GsWNiG0P2P355W5r6dIpn7FFRGRJRHhnBwfAaDjJySd9Xb
vO4ZMJtMufXObaSj817lVXc6UMLzAgMBAAECgYAOMLF4SvtrybH4XYWZH6NoUAKH
ENS+BlJ0TFfd1iqda7yc+7MEUaoWX6NtWj0jbqJ4Ra8cdNqW02MMSmNOzKhoB0CI
1PcbTlCYimxY9+PYdC/ALddA3Xn5VgpL4zUFpgxCat19n9q5bQq9NrRYbKeV1163
Becdk+uSX+rYQ4NF4QJBAMgKus6YkuA1OoTcAtoQNu7YHAKdP0HHCWGuGVcCfrxT
vFf5/S9rfOZlXNFAIEHfqIInzYxZacOPeJjwoukIEzkCQQDxA7dDdp8VKNgD4Jyi
p0BauEY2dH2ObmS9SywHk75+98kwdtAgyXFje83ifNlm658rzJ2ZKvbOFPrAQFKW
keuLAkEAqhhli7I0ga0BG2OwkRdkfmcIOjjXRuklUwekyFV4yk4xQmVxIdc2Fvpt
mAf85cZ2FGYf441L31upYfKLs7aIEQJBAIQ29aIcwMhqz5XQ8hyIg9VAocPdw8MO
7FSrmEvP0QD2DXh4tVRHLdzVXeyRcPH08Cym4w3Tc/E5sRUmNChFhGECQQCNwI6H
fbpGoGk4spLuKQNAgc08At8k6BXOsmHJ+rYgDaYbfcebTvs+gEduRPSg5B/GoBFL
/J5tIc0ucjXst7K+
-----END RSA Private Key-----`
	pubPem := `-----BEGIN RSA Public Key-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC8VQExERfjO7XeLBCE/TpfXFLm
kFIPTAbFkAcp5Ac8khhMvl3wsJkKxOOJrWNQWDtAZZR8NrJSouI5uUP5xu6E+KUa
8tjGMvOhrFjYhtD9j9+eVua+nSKZ+xRURkSUR4ZwcHwGg4ycknfV27zuGTCbTLn1
zm2ko/Ne5VV3OlDC8wIDAQAB
-----END RSA Public Key-----
`

	data := "haha"
	fmt.Println("src:" + data)
	encryptData := RSAEncrypt([]byte(data), []byte(pubPem))

	srcData, e := RsaPKCS8Decrypt(encryptData, []byte(priPem))
	if e != nil {
		fmt.Println(e.Error())
		return
	}

	fmt.Println("result:" + string(srcData))

}
