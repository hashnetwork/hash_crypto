package rsaUtils

import (
	"encoding/base64"
	"fmt"
	"testing"
)

func TestRsaSign(t *testing.T) {
	pri, pub, e := GenWithPKCS8(1024)
	if e != nil {
		fmt.Println(e.Error())
		return
	}

	data := "haha"
	sign, e1 := RsaPKCS8Sign([]byte(data), pri)
	if e1 != nil {
		fmt.Println(e1.Error())
		return
	}
	signBase64 := base64.StdEncoding.EncodeToString(sign)
	fmt.Println("sign", signBase64)

	e2 := RSASha256Verify([]byte(data), signBase64, pub)
	if e2 != nil {
		fmt.Println("error:", e2.Error())
		return
	}

	fmt.Println("ok")

}
