// SHA1是指由美国国家安全局提出的一种加密散列函数。
// 它接受一个输入并产生一个160比特的哈希值输出。
// 此外，该函数产生的输出被转换为40位的十六进制数字。
// 它被称为美国联邦信息处理标准。
// 它首次发布于1995年。它是1993年发布的SH0的继承者。
// 例如：
// Data : vsdiffer.com
// SHA1 : f54d63a4ca243d26cff5c3677ba2e2b14a2b47ce
// SHA-256是一个更安全、更新的加密哈希函数，于2000年作为SHA函数的新版本推出，并在2002年被采纳为FIPS标准。
// 它允许使用哈希生成器工具为任何字符串或输入值生成SHA256哈希值。
// 同时，它可以生成256个哈希值，内部状态大小为256位，原始信息大小最多为264-1位
// 例如：
// Data : vsdiffer.com
// SHA256 : d2d9f7b30866520a7d44d797bf8aaa24675d5c93a19050032df1d9566ad62a27
// 对比：
// 编号	     SHA1	                                    SHA256
// 1	  SHA1是SHA的第一个版本，产生一个160位的哈希值。	  SHA256是SHA2的类型，产生256位的哈希值。
// 2	  SHA1的内部状态大小为160。	                  SHA256的内部状态大小是256。
// 3	  与SHA256相比，SHA1的安全性较低。	              SHA256比SHA1更安全。
// 4	  SHA1的输出大小为160位。	                      SHA256的输出大小为256位。
// 5	  SHA1被SSL证书机构用来签署证书。	              SHA256是区块链中常用的哈希函数。
// 6	  SHA1的位数较小，所以它更容易受到攻击。	          SHA256有256位，所以它的安全性有所提高。

package hashUtil

import (
	"crypto/sha1"
	"crypto/sha256"
	"fmt"
)

func Sha1(data []byte) string {
	r := sha1.Sum(data)
	shaStr := fmt.Sprintf("%x", r)
	return shaStr
}

func Sha256(data []byte) string {
	sha := sha256.Sum256(data)
	shaStr := fmt.Sprintf("%x", sha)
	return shaStr
}
