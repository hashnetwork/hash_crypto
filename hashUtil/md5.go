package hashUtil

//一种被广泛使用的密码散列函数，可以产生出一个128位（16字节）的散列值（hash value），用于确保信息传输完整一致.
import (
	"crypto/md5"
	"fmt"
)

func Md5(data []byte) string {
	md := md5.Sum(data)
	mdStr := fmt.Sprintf("%x", md)
	return mdStr
}
