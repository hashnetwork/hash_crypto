package hashUtil

import (
	"crypto"
	"fmt"
	"testing"
)

func TestSha256(t *testing.T) {
	data := "haha"
	r := Sha256([]byte(data))
	fmt.Println(r)

	//----------------
	//1.选择hash算法，对需要签名的数据进行hash运算
	myhash := crypto.SHA256
	hashInstance := myhash.New()
	hashInstance.Write([]byte(data))
	hashed := hashInstance.Sum(nil)
	shaStr := fmt.Sprintf("%x", hashed)
	fmt.Println(shaStr)
}
